package casperix.mesh.city

import casperix.math.mesh.float32.MeshEdge
import casperix.math.test.FloatCompare.uniqueOrdered
import casperix.mesh.city.component.CityMeshBuilder
import casperix.mesh.city.component.CityMeshContext
import casperix.mesh.city.component.CityRegionBuilder

object CityMeshEdgeDivider {
    fun divide(context: CityMeshContext) {
        val divideToLength = context.config.divideToLength ?: return

        val last = context.meshBuilder.edges.all().filter { it.tag is CityRoadTag }
        context.meshBuilder.edges.removeAll(last)

        last.forEach {
            addSegments(context.builder, it, divideToLength)
        }
    }

    private fun addSegments(cityMeshBuilder: CityMeshBuilder, meshEdge: MeshEdge, divideToLength: Float): Boolean {
        val info = CityRegionBuilder.EdgeInfo.from(meshEdge.shape, divideToLength)
        val points = info.points.uniqueOrdered()
        var res = false
        (0 until points.size - 1).forEach { index ->
            val last = points[index]
            val next = points[index + 1]
            res = cityMeshBuilder.addRoad(MeshEdge(last, next, meshEdge.tag)) || res
        }
        return res

    }
}