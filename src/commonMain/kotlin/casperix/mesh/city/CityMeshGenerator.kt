package casperix.mesh.city

import casperix.math.mesh.MeshBuilder
import casperix.math.mesh.float32.Mesh
import casperix.mesh.city.component.*

class CityMeshGenerator(meshBuilder: MeshBuilder, config: CityMeshConfig) {

    private val context = CityMeshContext(meshBuilder, config)

    init {
        if (config.cityList.isNotEmpty()) {
            if (config.intercityRoads) CityMeshIntercityRoads.generate(context)
            if (config.primaryRoads) CityMeshPrimaryQuarters.generate(context)
            if (config.secondaryRoads) CityMeshSubQuarters.generate(context)
            if (config.postRemoveEdge) CityMeshPostRemoveEdge.generate(context)
            if (config.divideToLength != null) CityMeshEdgeDivider.divide(context)
        }
    }

    fun build(): Mesh {
        return context.builder.build().run { CityMeshSorter.sort(this) }
    }
}

