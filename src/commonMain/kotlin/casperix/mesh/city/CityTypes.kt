package casperix.mesh.city

import casperix.math.mesh.float32.MeshEdgeTag
import casperix.math.mesh.float32.MeshRegionTag
import casperix.math.vector.float32.Vector2f
import casperix.mesh.city.component.CityRegionConfig
import kotlin.random.Random


class City(val random: Random = Random(1), val pivot: Vector2f = Vector2f.ZERO, val grow: Int = 2, val regionConfig: CityRegionConfig = CityRegionConfig())

enum class RoadType {
    HIGHWAY,
    ARTERIAL,
    LOCAL_STREET,
    COLLECTOR_ROAD,
}
enum class Zone {
    RESIDENTIAL,
    RECREATION,
    INDUSTRIAL_LIGHT,
    INDUSTRIAL_MEDIUM,
    INDUSTRIAL_HEAVY,
    COMMERCIAL,
}

data class CityRoadTag(val level:Int, val roadType: RoadType, val city: City) : MeshEdgeTag
data class CityQuarterTag(val zone: Zone, val city: City) : MeshRegionTag