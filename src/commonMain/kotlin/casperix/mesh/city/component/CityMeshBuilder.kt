package casperix.mesh.city.component

import casperix.math.angle.float32.DegreeFloat
import casperix.math.axis_aligned.float32.getAABBox
import casperix.math.collection.getLooped
import casperix.math.geometry.Polygon2f
import casperix.math.geometry.Quad2f
import casperix.math.geometry.float32.Geometry2Float
import casperix.math.geometry.float32.grow
import casperix.math.geometry.float32.invert
import casperix.math.geometry.toSegment
import casperix.math.mesh.MeshBuilder
import casperix.math.mesh.float32.*
import casperix.math.straight_line.float32.LineSegment2f
import casperix.math.test.FloatCompare.uniqueOrdered
import casperix.math.vector.float32.Vector2f
import casperix.mesh.city.City
import casperix.mesh.city.CityRoadTag
import casperix.mesh.city.RoadType

class CityMeshBuilder(meshBuilder: MeshBuilder) : MeshBuilder by meshBuilder {
    private val epsilon = 0.1f
    private val magnetSize = 4f
    private val minDist = 0.5f
    private val minAngle = DegreeFloat(30f)

    fun addStickyQuarter(base: Quad2f, city: City, regionTag: MeshRegionTag?, depth: Int, roadType: RoadType): Boolean {
        val next = base.convert {
            nearPointInSegmentOrNull(it, magnetSize) ?: it
        }
        return addQuarter(next, regionTag, CityRoadTag(depth, roadType, city), city.regionConfig)
    }


    private fun addQuarter(quad: Polygon2f, regionTag: MeshRegionTag?, edgeTag: MeshEdgeTag?, cityRegionConfig: CityRegionConfig): Boolean {
        if (!isValid(quad)) return false
        if (!canPlaceQuarterRoads(quad)) return false
        if (!addRegion(quad, regionTag)) return false

        quad.getEdgeList().forEach {
            addRoad(MeshEdge(it.v0, it.v1, edgeTag))
        }
        quad.getVertices().forEach {
            addCross(MeshPoint(it))
        }

        return true
    }

    fun getRoadFrom(pivot: Vector2f): List<LineSegment2f> {
        return edges.search(pivot, epsilon).mapNotNull {
            if (it.tag is CityRoadTag) {
                val shape = it.shape
                if (shape.start.distTo(pivot) < shape.finish.distTo(pivot)) {
                    shape
                } else {
                    shape.invert()
                }
            } else null
        }
    }
    fun getRoadAround(pivot: Vector2f): List<Pair<MeshEdge, CityRoadTag>> {
        return edges.search(pivot, epsilon).mapNotNull {
            val tag = it.tag
            if (tag is CityRoadTag) {
                Pair(it, tag)
            } else null
        }
    }

    private fun canPlaceQuarterRoads(area: Polygon2f): Boolean {
        area.getVertices().forEachIndexed { index, it ->
            val selfA = area.getEdgeList().getLooped(index - 1).invert().toSegment()
            val selfB = area.getEdgeList().getLooped(index).toSegment()
            val exist = (getRoadFrom(it) + selfA + selfB).toSet().toList()

            if (!isValidAngles(exist)) {
                return false
            }
        }
        return true
    }

    fun addRegion(area: Polygon2f, regionTag: MeshRegionTag?): Boolean {
        if (!isValid(area)) return false

        val candidate = area

        val roads = edges.search(area.grow(-epsilon)).filter { it.tag is CityRoadTag }
        if (roads.isNotEmpty()) return false

        val quarters = regions.search(area.grow(-epsilon))//.filter { it.tag is CityQuarterTag }
        if (quarters.isNotEmpty()) return false

        add(MeshRegion(candidate, regionTag))
        return true
    }

    private fun isValidAngles(segments: List<LineSegment2f>): Boolean {
        val directions = segments.map { it.direction() }
        directions.forEachIndexed { xId, x ->
            directions.forEachIndexed { yId, y ->
                if (xId > yId) {
                    if (DegreeFloat.betweenDirections(x, y) < minAngle) {
                        return false
                    }
                }
            }
        }
        return true
    }

    private fun isValid(value: Polygon2f): Boolean {
        val vertices = value.getVertices()
        val set = vertices.toSet()

        vertices.forEachIndexed { xId, x ->
            vertices.forEachIndexed { yId, y ->
                if (xId > yId) {
                    if (x.distTo(y) < minDist) {
                        return false
                    }
                }
            }
        }

        set.forEach {
            if (!isValidPoint(it)) return false
        }
        return true
    }

    private fun isValidPoint(value: Vector2f): Boolean {
        return value.isFinite()
    }

    fun addCross(value: MeshPoint): Boolean {
        if (!isValidPoint(value.shape)) return false

        if (points.has(value)) return false
        add(value)
        return true
    }

    fun addRoad(start: Vector2f, finish: Vector2f, depth: Int, roadType: RoadType, city: City): Boolean {
        return addRoad(MeshEdge(start, finish, CityRoadTag(depth, roadType, city)))
    }

    fun addRoad(value: MeshEdge): Boolean {
        if (!isValidPoint(value.shape.start)) return false
        if (!isValidPoint(value.shape.finish)) return false

        if (edges.search(value.shape.getAABBox()).any {
                hasConflict(value.shape, it.shape)
            }) return false

        edges.add(value)
        return true
    }

    private fun hasConflict(a: LineSegment2f, b: LineSegment2f): Boolean {
        val a1 = Geometry2Float.distPointToSegment(a.start, b.toLine()) < epsilon &&
                Geometry2Float.distPointToSegment(a.finish, b.toLine()) < epsilon

        val a2 = Geometry2Float.distPointToSegment(b.start, a.toLine()) < epsilon &&
                Geometry2Float.distPointToSegment(b.finish, a.toLine()) < epsilon

        return a1 || a2
    }

    fun addMesh(subMesh: Mesh) {
        subMesh.edges.forEach {
            edges.add(it)
        }
        subMesh.points.forEach {
            points.add(it)
        }
        subMesh.regions.forEach {
            regions.add(it)
        }
    }
}