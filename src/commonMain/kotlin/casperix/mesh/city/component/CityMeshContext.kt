package casperix.mesh.city.component

import casperix.math.mesh.MeshBuilder
import casperix.mesh.city.City


class CityMeshContext(val meshBuilder: MeshBuilder, val config: CityMeshConfig) {
    val builder = CityMeshBuilder(meshBuilder)
}

data class CityMeshConfig(
    val cityList: List<City>,
    val intercityRoads: Boolean = true,
    val primaryRoads: Boolean = true,
    val secondaryRoads: Boolean = true,
    val postRemoveEdge: Boolean = false,
    val primarySize: Float = 16f,
    val chanceForDispersion: Float = 0.2f,
    val roadDegreeDispersion: Float = 10f,
    val roadDistanceDispersion: Float = 0.2f,
    val maxDepth: Int = 8,
    val divideToLength:Float? = 2f,
)