package casperix.mesh.city.component

import casperix.math.interpolation.float32.InterpolationFloat
import casperix.math.mesh.float32.UnorderedSegment
import casperix.math.random.nextDegreeFloat
import casperix.math.random.nextFloat
import casperix.mesh.city.RoadType
import kotlin.math.roundToInt

object CityMeshIntercityRoads {
    fun generate(context: CityMeshContext) = context.config.apply {
        val all = IntercityRoadsGenerator.generateRoads(PointBasedPathConfig(), cityList.map { it.pivot })
        val uniqueSegments = all.map {
            UnorderedSegment(it)
        }.toSet().map { it.basis }


        uniqueSegments.forEach { segment ->
            val dist = segment.length()
            val segments = (dist / primarySize).roundToInt()

            val points = (0..segments).map {
                InterpolationFloat.vector2(segment.start, segment.finish, it.toFloat() / segments)
            }

            val firstCity = cityList.minBy { it.pivot.distTo(points[0]) }

            var polars = (0 until segments).map {
                (points[it + 1] - points[it]).toPolar()
            }
            polars = polars.map {
                val dAngle = firstCity.random.nextDegreeFloat(-roadDegreeDispersion, roadDegreeDispersion)
                val dRange = firstCity.random.nextFloat(1f - roadDistanceDispersion, 1f + roadDistanceDispersion)
                it.copy(angle = it.angle + dAngle.toRadian(), range = it.range * dRange)
            }

            var offset = points[0]

            val prefinal = listOf(offset) + polars.map {
                offset += it.toDecart()
                offset
            }

            val compensate = prefinal.last() - points.last()

            val finalPoints = prefinal.mapIndexed { index, value ->
                val factor = index.toFloat() / (prefinal.size - 1)
                value - compensate * factor
            }

            (0 until segments).forEach { index ->
                val nearCity = cityList.minBy { it.pivot.distTo(finalPoints[index]) }
                context.builder.addRoad(finalPoints[index], finalPoints[index + 1], context.config.maxDepth, RoadType.HIGHWAY, nearCity)
            }
        }
    }

}