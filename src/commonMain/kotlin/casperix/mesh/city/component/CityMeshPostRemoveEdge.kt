package casperix.mesh.city.component

import casperix.mesh.city.City
import casperix.mesh.city.CityRoadTag
import casperix.mesh.city.RoadType
import casperix.misc.mapping
import casperix.misc.nextWeightedOrNull
import kotlin.math.pow
import kotlin.math.roundToInt

object CityMeshPostRemoveEdge {
    fun generate(context: CityMeshContext) = context.apply {
        context.config.cityList.forEach { city ->
            removeRandomEdges(context, city)
        }
    }

    private fun removeRandomEdges(context: CityMeshContext, city: City) = context.apply {
        val cityEdges = builder.edges.all().filter {
            (it.tag as? CityRoadTag)?.city == city
        }

        val chanceMap = cityEdges.mapping {

            val tag = it.tag as? CityRoadTag

            val levelFactor = when (tag?.roadType) {
                RoadType.HIGHWAY -> 1f
                RoadType.ARTERIAL -> 4f
                RoadType.LOCAL_STREET -> 8f
                RoadType.COLLECTOR_ROAD -> 16f
                else -> 0f
            }
            val distFactor = it.shape.median().distTo(city.pivot).pow(2)

            Pair(it, (levelFactor * distFactor).toDouble())
        }.toMutableMap()

        val removeAmount = (chanceMap.size * 0.7f).roundToInt()

        repeat(removeAmount) {
            val edge = city.random.nextWeightedOrNull(chanceMap)!!
            chanceMap.remove(edge)
            builder.edges.remove(edge)
        }
    }
}