package casperix.mesh.city.component

import casperix.math.angle.float32.DegreeFloat
import casperix.math.angle.float32.RadianFloat
import casperix.math.axis_aligned.float32.Box2f
import casperix.math.geometry.Quad2f
import casperix.math.geometry.float32.rotate
import casperix.math.geometry.float32.translate
import casperix.math.polar.float32.PolarCoordinateFloat
import casperix.math.polar.polarCoordinateOf
import casperix.math.random.nextDegreeFloat
import casperix.math.random.nextFloat
import casperix.math.random.nextVector2f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.rotateCW
import casperix.math.vector.toQuad
import casperix.mesh.city.City
import casperix.mesh.city.CityRoadTag
import casperix.mesh.city.RoadType
import casperix.misc.mapping
import casperix.misc.nextWeightedOrNull

object CityMeshPrimaryQuarters {
    fun generate(context: CityMeshContext) = context.config.apply {
        if (cityList.size <= 1) {
            generateAllBaseQuarters(context)
        }
        generateAllPrimaryQuarters(context)

    }

    private fun generateAllBaseQuarters(context: CityMeshContext) {
        context.config.cityList.forEach { city ->
            generateBaseQuarter(context, city)

        }
    }

    private fun generateAllPrimaryQuarters(context: CityMeshContext) = context.config.apply {
        val maxGrow = cityList.map { it.grow }.max()
        repeat(maxGrow) { growStep ->
            cityList.forEach { city ->
                if (city.grow <= growStep) return@forEach
                generatePrimaryQuarter(context, city)
            }
        }
    }


    private fun generatePrimaryQuarter(context: CityMeshContext, city: City): Unit = context.config.run {
        var remains = 100
        val random = city.random
        val builder = context.builder
        val cityRoads = builder.edges.all().map {
            Pair(it.tag as? CityRoadTag, it)
        }.filter { it.first?.city == city }.map { it.second }.mapping {
            val distFactor = 1.0 / (1.0 + it.shape.median().distTo(city.pivot))
            val tag = it.tag as CityRoadTag
            val levelFactor = when (tag.roadType) {
                RoadType.COLLECTOR_ROAD -> 1.0
                RoadType.HIGHWAY -> 2.0
                RoadType.LOCAL_STREET -> 8.0
                RoadType.ARTERIAL -> 16.0
                else -> 0.0
            }

            Pair(it, levelFactor * distFactor)
        }

        if (cityRoads.isEmpty()) {
            return
        }


        do {

            val segment = random.nextWeightedOrNull(cityRoads)!!.shape

            val basis = if (random.nextBoolean()) segment.invert() else segment

            val baseRight = PolarCoordinateFloat(primarySize, RadianFloat.byDirection(basis.delta().rotateCW().normalize()))

            var right1 = baseRight.toDecart()
            if (random.nextFloat() < chanceForDispersion) {
                val dist = right1.length() * random.nextFloat(1f - roadDistanceDispersion, 1f + roadDistanceDispersion)
                val angle = right1.toPolar().angle + DegreeFloat(random.nextFloat(-roadDegreeDispersion, roadDegreeDispersion)).toRadian()
                right1 = polarCoordinateOf(dist, angle).toDecart()
            }
            var right2 = baseRight.toDecart()
            if (random.nextFloat() < chanceForDispersion) {
                val dist = right2.length() * random.nextFloat(1f - roadDistanceDispersion, 1f + roadDistanceDispersion)
                val angle = right2.toPolar().angle + DegreeFloat(random.nextFloat(-roadDegreeDispersion, roadDegreeDispersion)).toRadian()
                right2 = polarCoordinateOf(dist, angle).toDecart()
            }

            val base = Quad2f(segment.start, segment.start + right1, segment.finish + right2, segment.finish)
            val result = builder.addStickyQuarter(base, city, null, context.config.maxDepth - 1, RoadType.ARTERIAL)
        } while (!result && remains-- >= 0)
    }

    private fun generateBaseQuarter(context: CityMeshContext, city: City) = context.config.apply {
        var remains = 100
        var offset = Vector2f.ZERO
        do {
            val actualSize = Vector2f(primarySize)
            val quarter = Box2f.byRadius(Vector2f.ZERO, actualSize / 2f)
                .toQuad()
                .rotate(city.random.nextDegreeFloat())
                .translate(city.pivot + offset)
            val result = context.builder.addStickyQuarter(quarter, city, null, context.config.maxDepth - 1, RoadType.ARTERIAL)
            if (!result) {
                offset += city.random.nextVector2f(-primarySize, primarySize)
            }
        } while (!result && remains-- >= 0)
    }

}