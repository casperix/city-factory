package casperix.mesh.city.component

import casperix.math.mesh.float32.Mesh
import casperix.math.mesh.float32.MeshEdgeTag
import casperix.mesh.city.CityRoadTag
import casperix.mesh.city.RoadType

object CityMeshSorter {
    fun sort(mesh: Mesh): Mesh {
        return mesh.copy(edges = mesh.edges.sortedBy {
            getTagLevel(it.tag)
        })
    }

    private fun getTagLevel(edge: MeshEdgeTag?): Int {
        return if (edge is CityRoadTag) {
            when (edge.roadType) {
                RoadType.HIGHWAY -> 4
                RoadType.ARTERIAL -> 3
                RoadType.LOCAL_STREET -> 2
                RoadType.COLLECTOR_ROAD -> 1
            }
        } else {
            0
        }
    }
}