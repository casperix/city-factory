package casperix.mesh.city.component

import casperix.math.geometry.Quad2f
import casperix.mesh.city.CityRoadTag

object CityMeshSubQuarters {
    fun generate(context: CityMeshContext) = context.apply {
        generateDepth(context)
    }

    private fun generateDepth(context: CityMeshContext) =
        context.apply {
            val quads = builder.regions.all().mapNotNull { Quad2f.from(it.shape.getVertices()) }
            builder.regions.clear()

            quads.forEach { shape ->
                val edge = builder.nearEdgeOrNull(shape.v0, 1f) ?: return@forEach
                val detail = edge.tag as? CityRoadTag ?: return@forEach
                val city = detail.city
                CityRegionBuilder.build(builder, city, shape, 8, context.config.primarySize)

            }

        }


}