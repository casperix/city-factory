package casperix.mesh.city.component

import casperix.math.collection.getLooped
import casperix.math.geometry.CustomPolygon2f
import casperix.math.geometry.Polygon2f
import casperix.math.geometry.RotateDirection
import casperix.math.geometry.float32.isConvex
import casperix.math.geometry.float32.median
import casperix.math.geometry.float32.setWindingOrder
import casperix.math.geometry.toSegment
import casperix.math.intersection.float32.Intersection2Float
import casperix.math.mesh.MeshBuilder
import casperix.math.mesh.float32.Mesh
import casperix.math.mesh.float32.MeshEdge
import casperix.math.straight_line.float32.LineSegment2f
import casperix.math.test.FloatCompare.uniqueOrdered
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.rotateCW
import casperix.mesh.city.*
import kotlin.math.max
import kotlin.math.roundToInt


object CityRegionBuilder {
    data class RegionBuilderInfo(
        val context: CityMeshBuilder,
        val city: City,
        val polygon: Polygon2f,
        val subPolygons: List<Polygon2f>?,
        val depth: Int,
        val minEdgeLength: Float,
        val primarySize: Float,
    )

    class EdgeInfo(val sections: Int, val basis: LineSegment2f) {

        companion object {

            fun from(segment: LineSegment2f, divideToLength: Float): EdgeInfo {
                return EdgeInfo(max(1, (segment.length() / divideToLength).roundToInt()), segment)

            }
        }

        init {
            if (sections <= 0) {
                throw Exception("Sections amount must be positive (1 or more)")
            }
        }

        val points = (0..sections).map {
            basis.getPosition(it / sections.toFloat())

        }

        fun median(): Vector2f {
            return points[sections / 2]
        }

        fun near(base: Vector2f): Vector2f {
            return points.minByOrNull { it.distTo(base) }!!
        }
    }

    val defaultRoadWidth: (RoadType) -> Float = {
        when (it) {
            RoadType.HIGHWAY -> 0.4f
            RoadType.ARTERIAL -> 0.3f
            RoadType.LOCAL_STREET -> 0.2f
            RoadType.COLLECTOR_ROAD -> 0.1f
        }
    }

    val defaultRoadByDepth: (Int) -> RoadType = {
        if (it >= 6) {
            RoadType.ARTERIAL
        } else if (it >= 4) {
            RoadType.LOCAL_STREET
        } else {
            RoadType.COLLECTOR_ROAD
        }
    }

    val defaultZoneBuilder: (RegionBuilderInfo) -> Zone? = { info ->
        val factor1 = info.primarySize / 8f
        val factor2 = info.primarySize / 6f
        val factor3 = info.primarySize / 4f

        val canSubdivide = info.subPolygons != null
        val isSmallArea = info.minEdgeLength <= factor1
        val isMediumArea = info.minEdgeLength <= factor2
        val isBigArea = info.minEdgeLength <= factor3

        if (isSmallArea || !canSubdivide) {
            getRandomResidentialOrCommercial(info.context, info.city, info.polygon)
        } else if (isMediumArea) {
            if (info.city.random.nextFloat() <= 0.2) {
                if (info.city.random.nextBoolean()) {
                    Zone.INDUSTRIAL_HEAVY
                } else {
                    Zone.INDUSTRIAL_MEDIUM
                }
            } else {
                null
            }
        } else if (isBigArea) {
            if (info.city.random.nextFloat() <= 0.1) {
                if (info.city.random.nextBoolean()) {
                    Zone.INDUSTRIAL_LIGHT
                } else {
                    Zone.RECREATION
                }
            } else {
                null
            }
        } else {
            null
        }
    }

    private fun getRandomResidentialOrCommercial(builder: CityMeshBuilder, city: City, polygon: Polygon2f): Zone {
        val random = city.random
        val vertices = polygon.getVertices()
        val length = vertices[0].distTo(vertices[vertices.size / 2])

        val center = polygon.median()

        val nearArterial =
            null != builder.edges.search(center, length * 0.6f).firstOrNull { (it.tag as? CityRoadTag)?.roadType == RoadType.ARTERIAL }
        val nearHighway =
            null != builder.edges.search(center, length * 0.6f).firstOrNull { (it.tag as? CityRoadTag)?.roadType == RoadType.HIGHWAY }

        val aroundHighway =
            null != builder.edges.search(center, length * 1.2f).firstOrNull { (it.tag as? CityRoadTag)?.roadType == RoadType.HIGHWAY }

        val chanceForCommercial = if (nearHighway) {
            0.95f
        } else if (nearArterial && aroundHighway) {
            0.80f
        } else if (nearArterial) {
            0.15f
        } else {
            0f
        }

        return if (random.nextFloat() <= chanceForCommercial) {
            Zone.COMMERCIAL
        } else {
            Zone.RESIDENTIAL
        }
    }


    fun build(meshBuilder: MeshBuilder, city: City, polygon: Polygon2f, primarySize: Float): Mesh {
        return build(meshBuilder, city, listOf(polygon), primarySize)
    }

    fun build(meshBuilder: MeshBuilder, city: City, polygonList: List<Polygon2f>, primarySize: Float): Mesh {
        val builder = CityMeshBuilder(meshBuilder)

        polygonList.forEach { polygon ->
            build(builder, city, polygon, city.regionConfig.maxDepth, primarySize)
        }

        return builder.build()
    }


    fun build(builder: CityMeshBuilder, city: City, polygon: Polygon2f, depth: Int, primarySize: Float) {
        val cityRegionConfig = city.regionConfig

        if (!isValid(polygon, cityRegionConfig.minLength)) {
            return
        }

        val edgeInfoList = polygon.getEdgeList().mapIndexed { index, line ->
            val segment = line.toSegment()
            EdgeInfo.from(segment, cityRegionConfig.baseLength)
        }

        val summaryPoints = edgeInfoList.flatMap {
            it.points
        }.uniqueOrdered()


        polygon.getEdgeList().forEach { edge ->
            builder.addRoad(MeshEdge(edge.toSegment(), CityRoadTag(depth, cityRegionConfig.getRoadType(depth), city)))
        }

        val minEdge = summaryPoints.mapIndexed { index, first ->
            val last = summaryPoints.getLooped(index + summaryPoints.size / 2)
            LineSegment2f(first, last)
        }.minBy { it.start.distTo(it.finish) }


        if (summaryPoints.size >= 7) {
            val nextDepth = depth - 1
            if (minEdge.length() >= cityRegionConfig.minLength && nextDepth > 0) {
                val subPolygonList = ShapeBuilder.dividePolygon(polygon, minEdge.grow(1f, 1f))

                if (subPolygonList != null && subPolygonList.size == 2) {
                    val zone = cityRegionConfig.getZone(
                        RegionBuilderInfo(
                            builder,
                            city,
                            polygon,
                            subPolygonList,
                            depth,
                            minEdge.length(),
                            primarySize
                        )
                    )
                    if (zone != null) {
                        val regionArea = reduceShapeByRoads(builder, cityRegionConfig, polygon)
                        builder.addRegion(regionArea, CityQuarterTag(zone, city))
                    } else {
                        subPolygonList.forEach {
                            build(builder, city, it, nextDepth, primarySize)
                        }
                    }
                    return
                }
            }
        }

        //  Если регион не делится -- ставим зону
        val zone = cityRegionConfig.getZone(RegionBuilderInfo(builder, city, polygon, null, depth, minEdge.length(), primarySize))
        if (zone != null) {
            val regionArea = reduceShapeByRoads(builder, cityRegionConfig, polygon)
            builder.addRegion(regionArea, CityQuarterTag(zone, city))
        }
    }

    private fun reduceShapeByRoads(builder: CityMeshBuilder, config: CityRegionConfig, shape: Polygon2f): Polygon2f {

        val shapeNormalized = shape.setWindingOrder(RotateDirection.CLOCKWISE)

        val edges = shapeNormalized.getEdgeList().map {
            val segment = it.toSegment()
            val edgeMedian = segment.median()

            val road = builder.getRoadAround(edgeMedian)
            val range = road.maxOfOrNull { (edge, tag) ->
                config.getRoadWidth(tag.roadType)
            } ?: 1f

            val offset = segment.direction().rotateCW() * range

            segment.toLine().convert { it + offset }
        }

        val vertices = edges.mapIndexed { index, edge ->
            val next = edges.getLooped(index + 1)
            Intersection2Float.getSegmentWithSegment(edge, next) ?: edge.v1
        }

        return CustomPolygon2f(vertices)
    }

    private fun isValid(polygon: Polygon2f, minEdgeLength: Float): Boolean {
        if (!polygon.isConvex()) {
            return false
        }
        val unique = polygon.getVertices().uniqueOrdered(minEdgeLength)
        return unique.size == polygon.getVertexAmount()
    }
}