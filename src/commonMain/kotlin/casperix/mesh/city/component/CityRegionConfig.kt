package casperix.mesh.city.component

import casperix.mesh.city.RoadType
import casperix.mesh.city.Zone

class CityRegionConfig(
    val minLength: Float = 0.5f,
    val baseLength: Float = 1f,
    val maxDepth: Int = 8,
    val getRoadWidth: (RoadType) -> Float = CityRegionBuilder.defaultRoadWidth,
    val getRoadType: (Int) -> RoadType = CityRegionBuilder.defaultRoadByDepth,
    val getZone: (CityRegionBuilder.RegionBuilderInfo) -> Zone? = CityRegionBuilder.defaultZoneBuilder,
)

//class CityZoneConfig(
//    val minLength: Float,
//    val zone:Zone,
//)