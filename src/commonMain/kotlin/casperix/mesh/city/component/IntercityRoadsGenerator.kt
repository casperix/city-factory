package casperix.mesh.city.component

import casperix.math.straight_line.float32.LineSegment2f
import casperix.math.vector.float32.Vector2f
import casperix.path.concrete.PathFactory

object IntercityRoadsGenerator {


    class Context(val config: PointBasedPathConfig, val points: List<Vector2f>) {
        val edges = mutableSetOf<LineSegment2f>()

        fun addEdges(nodes: List<Vector2f>) {
            val nextPath = pathToRoads(nodes).flatMap {
                listOf(it, LineSegment2f(it.finish, it.start))
            }

            nextPath.forEach { next ->
                if (edges.add(next)) {
                }
            }
        }

        private fun pathToRoads(nodes: List<Vector2f>): List<LineSegment2f> {
            return (1 until nodes.size).map {
                LineSegment2f(nodes[it - 1], nodes[it])
            }
        }

    }

    fun generateRoads(config: PointBasedPathConfig, points: List<Vector2f>):  List<LineSegment2f> {
        val context = Context(config, points)

        points.forEach {
            generateRoadFrom(context, it)
        }
        return context.edges.toList()
    }

    private fun generateRoadFrom(context: Context, main: Vector2f) {
        context.points.sortedBy { main.distTo(it) }.forEach { target ->
            if (main != target) {
                val factory = PathFactory(PointBasedPath(context.config, context.points, context.edges, target), main)
                factory.path?.let {
                    context.addEdges(it.nodes)
                }
            }
        }
    }


}