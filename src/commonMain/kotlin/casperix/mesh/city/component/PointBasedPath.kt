package casperix.mesh.city.component

import casperix.math.straight_line.float32.LineSegment2f
import casperix.math.vector.float32.Vector2f
import casperix.path.api.NodeEstimator
import kotlin.math.roundToInt

enum class LengthMode {
    DEFAULT,
    SQUARED,
    INF,
    ONE,
}

data class PointBasedPathConfig(
    val heuristicFactor: Float = 30f,
    val wayFactor: Float = 30f,
    val noWayFactor: Float = 100f,
    val lengthMode: LengthMode = LengthMode.DEFAULT,
)

class PointBasedPath(val config: PointBasedPathConfig, val points: List<Vector2f>, val ways: Set<LineSegment2f>, val target: Vector2f) :
    NodeEstimator<Vector2f> {

    init {
        if (config.wayFactor < 0f) {
            throw Exception("Expected non negative way factor. Now: ${config.wayFactor}")
        }
        if (config.noWayFactor < 0f) {
            throw Exception("Expected non negative no-way factor. Now: ${config.noWayFactor}")
        }
        if (config.heuristicFactor < 0f) {
            throw Exception("Expected non negative heuristic factor. Now: ${config.heuristicFactor}")
        }
    }

    override fun getHeuristic(current: Vector2f): Int {
        val lengthFactor = getLengthFactor(current - target)
        return (lengthFactor * config.heuristicFactor).roundToInt()
    }

    override fun getNeighbours(current: Vector2f): Collection<Vector2f> {
        return points
    }

    private fun getLengthFactor(delta: Vector2f): Float {
        return when (config.lengthMode) {
            LengthMode.DEFAULT -> delta.length()
            LengthMode.SQUARED -> delta.lengthSquared()
            LengthMode.INF -> delta.lengthInf()
            LengthMode.ONE -> delta.lengthOne()
        }
    }

    override fun getWeight(previous: Vector2f?, current: Vector2f, next: Vector2f): Int? {
        val edge = LineSegment2f(current, next)
        val hasRoad = ways.contains(edge)
        val lengthFactor = getLengthFactor(next - current)
        val wayFactor = if (hasRoad) {
            config.wayFactor
        } else {
            config.noWayFactor
        }

        return (lengthFactor * wayFactor).roundToInt()
    }

    override fun isFinish(previous: Vector2f?, current: Vector2f): Boolean {
        return (current == target)
    }

}