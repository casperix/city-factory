package casperix.mesh.city.component

import casperix.math.collection.getLooped
import casperix.math.color.Colors
import casperix.math.geometry.CustomPolygon
import casperix.math.geometry.PointAroundRay
import casperix.math.geometry.Polygon2f
import casperix.math.geometry.float32.Geometry2Float
import casperix.math.intersection.float32.Intersection2Float
import casperix.math.mesh.float32.ColorTag
import casperix.math.mesh.float32.Mesh
import casperix.math.mesh.float32.MeshRegion
import casperix.math.straight_line.float32.LineSegment2f
import casperix.math.test.FloatCompare
import casperix.math.vector.float32.Vector2f

object ShapeBuilder {

    val colors = listOf(Colors.RED, Colors.LIME, Colors.BLUE, Colors.YELLOW, Colors.CYAN, Colors.FUCHSIA, Colors.WHITE)

    fun create(shape: Polygon2f): Mesh {
        return Mesh(regions = listOf(MeshRegion(shape)))
    }

    fun divide(geometry: Mesh, segment: LineSegment2f): Mesh {
        var counter = 0
        val regions = geometry.regions.flatMap {
            dividePolygon(it.shape, segment)?.map {
                MeshRegion(it, ColorTag(colors.getLooped(counter++)))
            } ?: listOf(it)
        }

        return Mesh(regions = regions)
    }

    fun MutableList<Vector2f>.addUnique(value: Vector2f) {
        val prev = firstOrNull { FloatCompare.isLike(it, value) }
        if (prev == null) {
            add(value)
        }
    }

    fun dividePolygon(polygon: Polygon2f, segment: LineSegment2f): List<Polygon2f>? {
        val uniqueIntersectionList = mutableListOf<Vector2f>()
        val singlePolygon = mutableListOf<Vector2f>()
        val leftPolygon = mutableListOf<Vector2f>()
        val rightPolygon = mutableListOf<Vector2f>()

        polygon.getVertices().forEachIndexed { index, current ->
            singlePolygon += current

            val pad = Geometry2Float.getPointAroundRay(segment.start, segment.finish, current, FloatCompare.defaultError)
            if (pad != PointAroundRay.RIGHT) {
                leftPolygon += current
            }
            if (pad != PointAroundRay.LEFT) {
                rightPolygon += current
            }

            val next = polygon.getVertices().getLooped(index + 1)

            val intersection = Intersection2Float.getSegmentWithSegment(LineSegment2f(current, next), segment)
            if (intersection != null) {
                uniqueIntersectionList.addUnique(intersection)
                if (!FloatCompare.isLike(current, intersection) && !FloatCompare.isLike(next, intersection)) {
                    singlePolygon += intersection
                    leftPolygon += intersection
                    rightPolygon += intersection
                }
            }
        }

        if (uniqueIntersectionList.isEmpty()) {
            return null
        }
        if (uniqueIntersectionList.size == 1) {
            return listOf(CustomPolygon(singlePolygon))
        }
        if (uniqueIntersectionList.size == 2) {
            return listOf(CustomPolygon(leftPolygon), CustomPolygon(rightPolygon.reversed()))
        }
        return null
    }
}