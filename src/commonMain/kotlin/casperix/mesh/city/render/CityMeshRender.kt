package casperix.mesh.city.render

import casperix.math.color.Color
import casperix.math.color.ColorCode
import casperix.math.color.Colors
import casperix.math.mesh.float32.ColorTag
import casperix.math.mesh.float32.Mesh
import casperix.mesh.city.CityQuarterTag
import casperix.mesh.city.CityRoadTag
import casperix.mesh.city.RoadType
import casperix.mesh.city.Zone
import casperix.renderer.Renderer2D

@OptIn(ExperimentalUnsignedTypes::class)
object CityMeshRender {
    private val DEFAULT_THICK = 0.1f
    private val CROSS_DIAMETER = 0.6f

    private val RECREATION_COLOR = ColorCode(0u, 150u, 0u, 255u)
    private val RESIDENTIAL_COLOR = ColorCode(0u, 250u, 50u, 255u)
    private val COMMERCIAL_COLOR = ColorCode(0u, 0u, 200u, 255u)
    private val INDUSTRIAL_LIGHT_COLOR = ColorCode(255u, 255u, 50u, 255u)
    private val INDUSTRIAL_MEDIUM_COLOR = ColorCode(200u, 200u, 0u, 255u)
    private val INDUSTRIAL_HEAVY_COLOR = ColorCode(120u, 120u, 0u, 255u)

    data class LineRenderConfig(val color: Color, val thick: Float)

    val roadConfigMap = mapOf(
        Pair(RoadType.HIGHWAY, LineRenderConfig(ColorCode(250u, 150u, 50u, 255u), 0.5f)),
        Pair(RoadType.ARTERIAL, LineRenderConfig(ColorCode(250u, 250u, 100u, 255u), 0.5f)),
        Pair(RoadType.LOCAL_STREET, LineRenderConfig(ColorCode(200u, 200u, 200u, 255u), 0.3f)),
        Pair(RoadType.COLLECTOR_ROAD, LineRenderConfig(ColorCode(200u, 200u, 200u, 255u), 0.1f)),
    )

    fun render(
        renderer: Renderer2D,
        mesh: Mesh,
        drawCross: Boolean,
        drawEdges: Boolean,
        drawRegions: Boolean,
    ) {
        if (drawRegions) {
            mesh.regions.forEach {
                val tag = it.tag

                if (tag is ColorTag) {
                    renderer.drawPolygon(tag.color, it.shape)
                }
                if (tag is CityQuarterTag) {
                    val color = when (tag.zone) {
                        Zone.RECREATION -> RECREATION_COLOR
                        Zone.RESIDENTIAL -> RESIDENTIAL_COLOR
                        Zone.COMMERCIAL -> COMMERCIAL_COLOR
                        Zone.INDUSTRIAL_LIGHT -> INDUSTRIAL_LIGHT_COLOR
                        Zone.INDUSTRIAL_MEDIUM -> INDUSTRIAL_MEDIUM_COLOR
                        Zone.INDUSTRIAL_HEAVY -> INDUSTRIAL_HEAVY_COLOR
                    }
                    renderer.drawPolygon(color, it.shape)
                }
            }
        }
        if (drawEdges) {
            mesh.edges.forEach {
                val tag = it.tag
                if (tag is ColorTag) {
                    renderer.drawSegment(tag.color, it.shape, DEFAULT_THICK)
                }
                if (tag is CityRoadTag) {
                    val segment = it.shape.grow(-0.1f, -0.1f)
                    val config = roadConfigMap[tag.roadType] ?: throw Exception("Unsupported road type: ${tag.roadType}")
                    renderer.drawSegment(config.color, segment, config.thick)
                }
            }
        }
        if (drawCross) {
            mesh.points.forEach {
                renderer.drawPoint(Colors.RED, it.shape, CROSS_DIAMETER)
            }
        }
    }
}